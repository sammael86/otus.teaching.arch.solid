using System.Collections.Generic;
using Moq;
using otus.teaching.arch.solid.Abstractions;
using otus.teaching.arch.solid.Adapters;
using otus.teaching.arch.solid.Commands;
using otus.teaching.arch.solid.Exceptions;
using Xunit;

namespace otus.teaching.arch.solid.tests
{
    public class RotateCommandTests
    {
        private readonly Mock<IRotatable> _rotatableMock;
        private readonly Mock<IUniversalObject> _universalObjectMock;

        public RotateCommandTests()
        {
            _rotatableMock = new Mock<IRotatable>();
            _universalObjectMock = new Mock<IUniversalObject>();

            _universalObjectMock
                .SetupGet(uo => uo[nameof(IRotatable.Angle)])
                .Returns(0);
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IRotatable.AngularVelocity)])
                .Returns(3);
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IRotatable.MaximumAngles)])
                .Returns(8);
        }

        public static IEnumerable<object[]> RotateObject_ObjectRotated_TestData()
        {
            yield return new object[] { 0, 1, 8, 1 };
            yield return new object[] { 0, 5, 4, 1 };
            yield return new object[] { 2, -4, 8, 6 };
        }

        [Theory]
        [MemberData(nameof(RotateObject_ObjectRotated_TestData))]
        public void RotateObject_ObjectRotated_NewAngleSet(int angle, int angularVelocity, int maximumAngles,
            int newAngle)
        {
            // Arrange
            _rotatableMock
                .SetupGet(r => r.Angle)
                .Returns(angle)
                .Verifiable();
            _rotatableMock
                .Setup(r => r.AngularVelocity)
                .Returns(angularVelocity)
                .Verifiable();
            _rotatableMock
                .Setup(r => r.MaximumAngles)
                .Returns(maximumAngles)
                .Verifiable();
            _rotatableMock
                .SetupSet(r => r.Angle = It.Is<int>(a => a == newAngle))
                .Verifiable();

            var cmd = new RotateCommand(_rotatableMock.Object);

            // Act
            cmd.Execute();

            // Assert
            _rotatableMock.VerifyAll();
        }

        [Fact]
        public void RotateObject_AngleCannotBeGet_ThrowsException()
        {
            // Arrange
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IRotatable.Angle)])
                .Returns(null);

            var cmd = new RotateCommand(new RotatableAdapter(_universalObjectMock.Object));

            // Act

            // Assert
            Assert.Throws<GetAngleException>(() => cmd.Execute());
        }

        [Fact]
        public void RotateObject_AngularVelocityCannotBeGet_ThrowsException()
        {
            // Arrange
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IRotatable.AngularVelocity)])
                .Returns(null);

            var cmd = new RotateCommand(new RotatableAdapter(_universalObjectMock.Object));

            // Act

            // Assert
            Assert.Throws<GetAngularVelocityException>(() => cmd.Execute());
        }

        [Fact]
        public void RotateObject_MaximumAnglesCannotBeGet_ThrowsException()
        {
            // Arrange
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IRotatable.MaximumAngles)])
                .Returns(null);

            var cmd = new RotateCommand(new RotatableAdapter(_universalObjectMock.Object));

            // Act

            // Assert
            Assert.Throws<GetMaximumAnglesException>(() => cmd.Execute());
        }

        [Fact]
        public void RotateObject_AngleCannotBeSet_ThrowsException()
        {
            // Arrange
            _universalObjectMock
                .SetupSet(uo => uo[nameof(IRotatable.Angle)] = It.IsAny<int>())
                .Throws(new SetAngleException());

            var cmd = new RotateCommand(new RotatableAdapter(_universalObjectMock.Object));

            // Act

            // Assert
            Assert.Throws<SetAngleException>(() => cmd.Execute());
        }
    }
}