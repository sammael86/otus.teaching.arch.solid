using System.Collections.Generic;
using Moq;
using otus.teaching.arch.solid.Abstractions;
using otus.teaching.arch.solid.Adapters;
using otus.teaching.arch.solid.Commands;
using otus.teaching.arch.solid.Exceptions;
using otus.teaching.arch.solid.Models;
using Xunit;

namespace otus.teaching.arch.solid.tests
{
    public class MoveCommandTests
    {
        private readonly Mock<IMovable> _movableMock;
        private readonly Mock<IUniversalObject> _universalObjectMock;

        public MoveCommandTests()
        {
            _movableMock = new Mock<IMovable>();
            _universalObjectMock = new Mock<IUniversalObject>();

            _universalObjectMock
                .Setup(uo => uo[nameof(IMovable.Position)])
                .Returns(new Vector(new[] { 1, 1 }));
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IMovable.Velocity)])
                .Returns(new Vector(new[] { 1, 1 }));
        }

        public static IEnumerable<object[]> MoveObject_ObjectMoved_TestData()
        {
            yield return new object[]
            {
                new Vector(new[] { 12, 5 }),
                new Vector(new[] { -7, 3 }),
                new Vector(new[] { 5, 8 })
            };
            yield return new object[]
            {
                new Vector(new[] { 5, 1 }),
                new Vector(new[] { 3, 9 }),
                new Vector(new[] { 8, 10 })
            };
        }

        [Theory]
        [MemberData(nameof(MoveObject_ObjectMoved_TestData))]
        public void MoveObject_ObjectMoved_NewPositionSet(Vector position, Vector velocity, Vector newPosition)
        {
            // Arrange
            _movableMock
                .SetupGet(m => m.Position)
                .Returns(position)
                .Verifiable();
            _movableMock
                .Setup(m => m.Velocity)
                .Returns(velocity)
                .Verifiable();
            _movableMock
                .SetupSet(m => m.Position = It.Is<Vector>(p => p == newPosition))
                .Verifiable();

            var cmd = new MoveCommand(_movableMock.Object);

            // Act
            cmd.Execute();

            // Assert
            _movableMock.VerifyAll();
        }

        [Fact]
        public void MoveObject_PositionCannotBeGet_ThrowsException()
        {
            // Arrange
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IMovable.Position)])
                .Returns(null);

            var cmd = new MoveCommand(new MovableAdapter(_universalObjectMock.Object));

            // Act

            // Assert
            Assert.Throws<GetPositionException>(() => cmd.Execute());
        }

        [Fact]
        public void MoveObject_VelocityCannotBeGet_ThrowsException()
        {
            // Arrange
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IMovable.Velocity)])
                .Returns(null);

            var cmd = new MoveCommand(new MovableAdapter(_universalObjectMock.Object));

            // Act

            // Assert
            Assert.Throws<GetVelocityException>(() => cmd.Execute());
        }

        [Fact]
        public void MoveObject_PositionCannotBeSet_ThrowsException()
        {
            // Arrange
            _universalObjectMock
                .SetupSet(uo => uo[nameof(IMovable.Position)] = It.IsAny<Vector>())
                .Throws(new SetPositionException());

            var cmd = new MoveCommand(new MovableAdapter(_universalObjectMock.Object));

            // Act

            // Assert
            Assert.Throws<SetPositionException>(() => cmd.Execute());
        }
    }
}