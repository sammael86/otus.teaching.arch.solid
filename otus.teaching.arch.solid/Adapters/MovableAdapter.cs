using otus.teaching.arch.solid.Abstractions;
using otus.teaching.arch.solid.Exceptions;
using otus.teaching.arch.solid.Models;

namespace otus.teaching.arch.solid.Adapters
{
    public class MovableAdapter : IMovable
    {
        private readonly IUniversalObject _universalObject;

        public MovableAdapter(IUniversalObject universalObject)
        {
            _universalObject = universalObject;
        }

        public Vector Position
        {
            get
            {
                if (_universalObject[nameof(IMovable.Position)] is Vector position)
                    return position;

                throw new GetPositionException();
            }
            set
            {
                if (_universalObject[nameof(IMovable.Position)] is not Vector)
                    throw new SetPositionException();

                _universalObject[nameof(IMovable.Position)] = value;
            }
        }

        public Vector Velocity
        {
            get
            {
                if (_universalObject[nameof(IMovable.Velocity)] is Vector velocity)
                    return velocity;

                throw new GetVelocityException();
            }
            //set => throw new System.NotImplementedException();
        }
    }
}