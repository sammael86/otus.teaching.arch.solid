using otus.teaching.arch.solid.Abstractions;
using otus.teaching.arch.solid.Exceptions;

namespace otus.teaching.arch.solid.Adapters
{
    public class RotatableAdapter : IRotatable
    {
        private readonly IUniversalObject _universalObject;

        public RotatableAdapter(IUniversalObject universalObject)
        {
            _universalObject = universalObject;
        }

        public int Angle
        {
            get
            {
                if (_universalObject[nameof(IRotatable.Angle)] is int angle)
                    return angle;

                throw new GetAngleException();
            }
            set
            {
                if (_universalObject[nameof(IRotatable.Angle)] is not int)
                    throw new SetAngleException();

                _universalObject[nameof(IRotatable.Angle)] = value;
            }
        }

        public int AngularVelocity
        {
            get
            {
                if (_universalObject[nameof(IRotatable.AngularVelocity)] is int angularVelocity)
                    return angularVelocity;

                throw new GetAngularVelocityException();
            }
        }

        public int MaximumAngles
        {
            get
            {
                if (_universalObject[nameof(IRotatable.MaximumAngles)] is int maximumAngles)
                    return maximumAngles;

                throw new GetMaximumAnglesException();
            }
        }
    }
}