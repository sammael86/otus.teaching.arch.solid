namespace otus.teaching.arch.solid.Abstractions
{
    public interface IRotatable
    {
        int Angle { get; set; }
        int AngularVelocity { get; }
        int MaximumAngles { get; }
    }
}