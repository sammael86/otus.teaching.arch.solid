namespace otus.teaching.arch.solid.Abstractions
{
    public interface IUniversalObject
    {
        object this[string key] { get; set; }
    }
}