namespace otus.teaching.arch.solid.Abstractions
{
    public interface ICommand
    {
        void Execute();
    }
}