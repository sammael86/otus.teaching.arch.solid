using otus.teaching.arch.solid.Models;

namespace otus.teaching.arch.solid.Abstractions
{
    public interface IMovable
    {
        Vector Position { get; set; }
        Vector Velocity { get; }
    }
}